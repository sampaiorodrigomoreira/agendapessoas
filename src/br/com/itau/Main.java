package br.com.itau;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        Agenda agenda = new Agenda();

        agenda.adicionarContato();
        agenda.removerContato();
        agenda.mostrarAgenda();

    }
}
