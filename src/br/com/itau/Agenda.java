package br.com.itau;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Agenda {

    private ArrayList lista;

    ArrayList<String> agenda = new ArrayList();
    Scanner pessoa = new Scanner(System.in);

    public void adicionarPessoas(int quantidade) {
        for (int i = 0; i < quantidade; i++) {

            System.out.println("Informe a pessoa");
            lista.add(pessoa.nextInt());

        }
    }


    List<Contato> contatos;

    public Agenda() {
        contatos = new ArrayList<>();
    }

    public void adicionarContato() {

        int numeroPessoas = Integer.parseInt(JOptionPane.showInputDialog("Quantas pessoas deseja adicionar ao iniciar o sistema?"));

        for (int i = 1; i < numeroPessoas + 1; i++) {

            String nome = JOptionPane.showInputDialog("Nome do " + i + "° Contato: ");
            String email = JOptionPane.showInputDialog("Email do " + i + "° Contato: ");
            String telefone = JOptionPane.showInputDialog("Telefone do " + i + "° Contato: ");

            Contato contato = new Contato(nome, email, telefone);

            contatos.add(contato);
        }

        JOptionPane.showMessageDialog(null,"Inclusão com Sucesso!");

    }

    public void removerContato() {

        int opcao = Integer.parseInt(JOptionPane.showInputDialog("Informe 1 para remover por email ou 2 para remover pelo numero"));

        if (opcao == 1) {

            String email = JOptionPane.showInputDialog("Informe o email: ");

            for (Contato contato : contatos) {

                if(contato.getEmail().equals(email))
                contato.getEmail().replace(contato.getEmail(),"");

                JOptionPane.showMessageDialog(null,"email:  " + email + " removido com sucesso");

                //Impressao.imprimir(contato);

            }

        } else {
            System.out.println("Remoção do contato por telefone. ");

            for (Contato contato : contatos) {
                System.out.println(contato.getNome() + " " + contato.getEmail() + " " + contato.getTelefone());
            }
        }
        //contatos.remove(contato);

    }

    public void mostrarAgenda() {
        for (Contato contato : contatos) {

            JOptionPane.showMessageDialog(null,"Agenda: " + contato.getNome() + "\n" +
                    " " + contato.getEmail() + "\n" +
                    " " + contato.getTelefone());

        }
    }

}
